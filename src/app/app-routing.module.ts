import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthorGuard } from './guards/author.guard';

const routes: Routes = [
  { path: 'login', loadChildren: () => import('./components/login/login.module').then((m) => m.LoginModule), data: { showHeader: false, showFooter: false } },
  {
    path: '',
    loadChildren: () => import('./components/contents/contents.module').then((m) => m.ContentsModule),
    canActivate: [AuthorGuard]
  },
  { path: 'admin', loadChildren: () => import('./components/admin/admin.module').then((m) => m.AdminModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
