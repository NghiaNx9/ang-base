import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor() { }

  get isLoggedIn(): string | null {
    return localStorage.getItem('isLoggedIn');
  }

  set isLoggedIn(authToken: string | null) {
    if (authToken)
      localStorage.setItem('isLoggedIn', authToken);
  }

  logout() {
    localStorage.clear();
  }
}
