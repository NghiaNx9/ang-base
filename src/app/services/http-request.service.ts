import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpRequestService {
  private baseUrl = environment.BASE_URL;
  constructor(private http: HttpClient) { }

  callPostApi(uri: string, bodyRequest: any): Observable<any> {
    return this.http.post(this.baseUrl + uri, bodyRequest, {
      observe: 'response'
    });
  }
  callGetApi(uri: string, bodyRequest?: any): Observable<any> {
    const params = new HttpParams();
    if (bodyRequest && bodyRequest?.body) {
      for (const propertyName in bodyRequest) {
        params.set(propertyName, bodyRequest[propertyName]);
      }
    }
    return this.http.get(
      this.baseUrl + uri,
      {
        params: params,
        observe: 'response',
      },
    );
  }
  isResponseSuccess(res: any): boolean {
    return (
      res &&
      res.body &&
      res.body.statusCode === '200 OK' &&
      res.body.content
    );
  }
}
