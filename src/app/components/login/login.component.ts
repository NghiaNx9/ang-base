import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm = this.fb.group({
    username: '',
    password: '',
    remember: false
  })
  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }
  onSubmit() {
    this.loginForm.markAllAsTouched();
    if(this.loginForm.valid){
      this.authService.isLoggedIn = 'a213213aasdasd';
      this.router.navigateByUrl('/');
    }
  }
}
