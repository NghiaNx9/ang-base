import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthorGuard } from 'src/app/guards/author.guard';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: '', component: HomeComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContentsRoutingModule { }
